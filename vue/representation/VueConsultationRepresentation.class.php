<?php
/**
 * Description Page de consultation des offres d'hébergement par établissmeent
 * -> affiche une page comportant un tableau par établissement, indiquant 
 * pour chaque type de chambre, le nombre de chambres offertes pour cet établissment
 * @author prof
 * @version 2018
 */

namespace vue\representation;

use vue\VueGenerique;
use modele\metier\Representation;

class VueConsultationRepresentation extends VueGenerique {

    /** @var array liste des Representation */
    private $lesRepresentations;

    /** @var array liste des différent Lieus */
    private $lesLieux;
    
    /** @var array liste des groupes */
    private $lesGroupes;
    

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();

        // LIGNE D'EN-TÊTE ET 1 LIGNE PAR TYPE DE CHAMBRE
        $this->laDate = 0;
        foreach ($this->lesRepresentations as $uneRepresentation) {
            if($this->laDate != $uneRepresentation->getDateRep()){
                if($this->laDate != 0){?> </table><br/> <?php } ?>
                <strong><?= $uneRepresentation->getDateRep()?></strong><br>
                <table width="45%" cellspacing="0" cellpadding="0" class="tabQuadrille">
                    <!--AFFICHAGE DE LA LIGNE D'EN-TÊTE-->
                    <tr class="enTeteTabQuad">
                        <td width="30%">Lieu</td>
                        <td width="30%">Groupe</td>
                        <td width="30%">Heure Debut</td>
                        <td width="30%">Heure Fin</td>
                    </tr>
                        <tr class="ligneTabQuad">
                            <td><?= $uneRepresentation->getId_Lieu()->getNom() ?></td>
                            <td><?= $uneRepresentation->getId_Groupe()->getNom() ?></td>
                            <td><?= $uneRepresentation->getHeureDebut() ?></td>
                            <td><?= $uneRepresentation->getHeureFin() ?></td>
                        </tr>
                </table><br>
                <?php
            }    
        }
        include $this->getPied();
    }

    public function setLesRepresentations(array $lesRepresentations) {
        $this->lesRepresentations = $lesRepresentations;
    }   

}

