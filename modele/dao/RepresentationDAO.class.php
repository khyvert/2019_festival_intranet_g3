<?php

namespace modele\dao;

use modele\metier\Representation;
use modele\metier\Lieu;
use modele\metier\Groupe;
use PDO;

/**
 * Description of OffreDAO
 * Classe métier :  Offre
 * @author prof
 * @version 2017
 */
class RepresentationDAO {

    /**
     * crée un objet métier à partir d'un enregistrement de la table OFFRE et des tables liées
     * @param array $enreg Description
     * @return Offre objet métier obtenu
     */
    protected static function enregVersMetier($enreg) {
        $id = $enreg['ID'];
        $idLieu = $enreg['ID_LIEU'];
        $idGroupe = $enreg['ID_GROUPE'];
        $dateRep = $enreg['DATEREP'];
        $heureDebut = $enreg['HEUREDEBUT'];
        $heureFin = $enreg['HEUREFIN'];
        // construire les objets Etablissement et TypeChambre à partir de leur identifiant       
        $objetLieu = LieuDAO::getOneById($idLieu);
        $objetGroupe = GroupeDAO::getOneById($idGroupe);
        // instancier l'objet Offre
        $objetMetier = new Representation($id, $objetLieu, $objetGroupe, $dateRep, $heureDebut, $heureFin);

        return $objetMetier;
    }

    /**
     * Complète une requête préparée
     * les paramètres de la requête associés aux valeurs des attributs d'un objet métier
     * @param Offre $objetMetier
     * @param PDOStatement $stmt
     */
    protected static function metierVersEnreg(Representation $objetMetier, \PDOStatement $stmt) {
        // On utilise bindValue plutôt que bindParam pour éviter des variables intermédiaires
        /* @var $etab Etablissement */
        $lieu = $objetMetier->getLieu();
        /* @var $typeCh TypeChambre */
        $groupe = $objetMetier->getGroupe();
        $stmt->bindValue(':id', $objetMetier->getRepresentation());
        $stmt->bindValue(':id_lieu', $lieu->getIdLieu());
        $stmt->bindValue(':id_groupe', $groupe->getIdGroupe());
        $stmt->bindValue(':daterep', $dateRep->getDateRep());
        $stmt->bindValue(':heuredebut', $heureDebut->getHeureDebut());
        $stmt->bindValue(':heurefin', $heureFin->getHeureFin());
        
    }

    /**
     * Retourne la liste de toutes les offres
     * @return array tableau d'objets de type Offre
     */
    public static function getAll() {
        $lesObjets = array();
        $requete = "SELECT * FROM Representation";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }

    /**
     * Construire un objet d'après son identifiant, à partir des des enregistrements de la table Offre
     * L'identifiant de la table Offre est composé : ($idEtab, $idTypeChambre)
     * @param string $idEtab identifiant de l'établissement émetteur de l'offre
     * @param string $idTypeChambre identifiant du type de chambre concerné par l'offre
     * @return Offre : objet métier si trouvé dans la BDD, null sinon
     */
        public static function getOneById($id) {
        $objetConstruit = null;
        $requete = "SELECT * FROM Representation WHERE ID = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }

    /**
     * Détruire un enregistrement de la table OFFRE d'après son identifiant
     * @param string $idEtab identifiant de l'établissement émetteur de l'offre
     * @param string $idTypeCh identifiant du type de chambre concerné par l'offre
     * @return boolean =TRUE si l'enregistrement est détruit, =FALSE si l'opération échoue
     */
    public static function delete($id) {
        $ok = false;
        $requete = "DELETE FROM Representation "
                . " WHERE ID=:id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }

    /**
     * Insérer un nouvel enregistrement dans la table à partir de l'état d'un objet métier
     * @param Offre objet métier à insérer
     * @return boolean =FALSE si l'opération échoue
     */
    public static function insert(Representation $objet) {
        $ok = false;
        $requete = "INSERT INTO Representation "
                . "  VALUES(:id, :id_lieu, :id_groupe, :daterep, :heuredebut, :heurefin)";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }

 
    /**
     * Mise à jour du nombre de chambres associé à une offre
     * @param string $idEtab identifiant de l'établissement concerné par l'offre
     * @param string $idTypeCh identifiant du type de chambre concerné par l'offre
     * @param int $nb nouveau nombre de chambre 
     * @return boolean =true si la mise à jour a été correcte
     */
    public static function update($id, $objet) {
        $ok = false;
        $RepresentationLue = self::getOneById($id);
        $RepresentationLue->setRepresentation($objet);
        $requete = "UPDATE Representation "
                . " SET ID_LIEU=:id_lieu, ID_GROUPE=:id_groupe, DATEREP=:daterep, HEUREDEBUT=:heuredebut, HEUREFIN=:heurefin "
                . " WHERE ID=:id";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($RepresentationLue, $stmt);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }


}