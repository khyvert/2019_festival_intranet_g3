<?php
namespace modele\metier;

class Representation {
    
    private $id;
    
    private $id_lieu;
    
    private $id_groupe;
    
    private $daterep;
    
    private $heuredebut;
    
    private $heurefin;
    
    
    
    function __construct($id, $id_lieu, $id_groupe, $daterep, $heuredebut, $heurefin) {
        $this->id = $id;
        $this->id_lieu = $id_lieu;
        $this->id_groupe = $id_groupe;
        $this->daterep = $daterep;
        $this->heuredebut = $heuredebut;
        $this->heurefin = $heurefin;
        
    }
    
    function getId() {
        return $this->id;
    }

    function getid_Lieu() {
        return $this->id_lieu;
    }

    function getId_Groupe() {
        return $this->id_groupe;
    }

    function getDateRep() {
        return $this->daterep;
    }
    
    function getHeureDebut() {
        return $this->heuredebut;
    }
    
    function getHeureFin() {
        return $this->heurefin;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setId_Lieu($id_lieu) {
        $this->id_lieu = $id_lieu;
    }

    function setId_Groupe($id_groupe) {
        $this->id_groupe = $id_groupe;
    }
    
    function setDateRep($daterep) {
        $this->daterep = $daterep;
    }
    
    function setHeureDebut($heuredebut) {
        $this->heuredebut = $heuredebut;
    }
    
    function setHeureFin($heurefin) {
        $this->heurefin = $heurefin;
    }

}