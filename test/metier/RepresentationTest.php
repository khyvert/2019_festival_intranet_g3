<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Représentation Test</title>
    </head>
    <body>
        <?php
        use modele\metier\Representation;
        use modele\metier\Groupe;
        use modele\metier\Lieu;
        require_once __DIR__ . '/../../includes/autoload.inc.php';
        echo "<h2>Test unitaire de la classe métier Représentation</h2>";
        $unGroupe = new Groupe('g001', 'groupe folklorique du Bachkortostan', '', '', '40', 'Backirie', 'O');
        $unLieu = new Lieu('1', 'salle du panier fleuri', 'rue de bonneville', '450');
        $uneRepresentation = new Representation('1',$unLieu, $unGroupe, '2017-07-11', '21:00', '22:30');
        var_dump($uneRepresentation);
        ?>
    </body>
</html>