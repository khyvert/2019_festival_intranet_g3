<!doctype html>
<?php
use modele\dao\RepresentationDAO;
use modele\dao\Bdd;
use controleur\Session;

require_once __DIR__ . '/../../includes/autoload.inc.php';
        Session::demarrer();
        Bdd::connecter();
?>
<html>
    <head>
        <meta charset="utf-8">
        <title>RepresentationDAO : test</title>
    </head>

    <body>

        <?php

        // Jeu d'essai
        $id = '1';
        $idLieu = '1';
        $idGroupe = 'g001';
        $dateRep = '2017-07-11';
        $heureDebut = '20:00';
        $heureFin = '21:45';

        echo "<h2>Test de RepresentationDAO</h2>";

        // Test n°1
        echo "<h3>1- getOneById</h3>";
        $objet = RepresentationDAO::getOneById($id);
        var_dump($objet);

        // Test n°2
        echo "<h3>2- getAll</h3>";
        $lesObjets = RepresentationDAO::getAll();
        var_dump($lesObjets);

        // Test n°3
        echo "<h3>3- insert</h3>";
        try {
//            /* @var $unEtab modele\metier\Etablissement */
//            $unEtab = EtablissementDAO::getOneById($idEtab);
//            /* @var $unTypeCh modele\metier\TypeChambre */
//            $unTypeCh = TypeChambreDAO::getOneById($idTypeCh_2);
//            /* @var $objet modele\metier\Offre */
//            $objet = new Offre($unEtab, $unTypeCh, $nb_1);
            if ($ok) {
                echo "<h4>ooo réussite de l'insertion ooo</h4>";
//                $objetLu = OffreDAO::getOneById($unEtab->getId(), $unTypeCh->getId());
                $objetLu = RepresentationDAO::getOneById($id);
                var_dump($objetLu);
            } else {
                echo "<h4>*** échec de l'insertion ***</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
        }

        // Test n°4
        echo "<h3>4- update</h3>";
        try {
            $ok = RepresentationDAO::update($id, $idLieu, $idGroupe, $dateRep, $heureDebut, $heureFin);
            if ($ok) {
                $objetLu = RepresentationDAO::getOneById($id);
                if ($objetLu->getRepresentation() == $id) {
                    echo "<h4>ooo réussite de la mise à jour ooo</h4>";
                    var_dump($objetLu);
                } else {
                    echo "<h4>*** échec de la mise à jour, le nombre de chambres n'est pas le bon ***</h4>";
                }
            } else {
                echo "<h4>*** échec de la mise à jour, erreur DAO ***</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>*** échec de la requête, erreur PDO ***</h4>" . $e->getMessage();
        }

        // Test n°5
        echo "<h3>5- delete</h3>";
        try {
            $ok = RepresentationDAO::delete($id);
            if ($ok) {
                echo "<h4>ooo réussite de la suppression ooo</h4>";
            } else {
                echo "<h4>*** échec de la suppression ***</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
        }
        
        
        ?>

    </body>
</html>
