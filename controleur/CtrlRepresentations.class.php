<?php

/**
 * Contrôleur de gestion des offres d'hébergement
 * @author prof
 * @version 2018
 */

namespace controleur;

use modele\dao\RepresentationDAO;
use modele\dao\LieuDAO;
use modele\dao\GroupeDAO;
use modele\metier\Groupe;
use modele\metier\Lieu;
use modele\dao\Bdd;
use modele\metier\Representation;
use vue\representation\VueConsultationRepresentation;
use vue\representation\VueSaisieRepresentations;
use vue\representation\VueSupprimerRepresentations;

class CtrlRepresentations extends ControleurGenerique {
    
    /** controleur= offres & action= defaut
     * Afficher la liste des offres d'hébergement      */
    public function defaut() {
        $this->consulter();
    }

    
    function consulter() {
        $laVue = new VueConsultationRepresentation();
        $this->vue = $laVue;
        // On récupère un tableau composé de la liste des établissements avec, pour chacun,
        //  son nombre d'attributions de chambres actuel : 
        //  on ne peut supprimer un établissement que si aucune chambre ne lui est actuellement attribuée
        Bdd::connecter();
        $laVue->setLesRepresentations(RepresentationDAO::getAll());
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - representations");
        $this->vue->afficher();
    }
    
    /** controleur= representation & action=creer
     * Afficher le formulaire d'ajout d'un representation     */
    public function creer() {
        $laVue = new VueSaisieRepresentations();
        $this->vue = $laVue;
        $laVue->setActionRecue("creer");
        $laVue->setActionAEnvoyer("validerCreer");
        $laVue->setMessage("Nouvelle representation");
        $leGroupe = new Groupe("", "", "", "", "", "", "");
        $leLieu = new Lieu("", "", "", "");
        // En création, on affiche un formulaire vide
        /* @var representation $unEtab */
        $uneRep = new Representation("",$leLieu, $leGroupe,"", "", "");
        Bdd::connecter();
        $laVue->setLesLieux(LieuDAO::getAll());
        $laVue->setLesGroupes(GroupeDAO::getAll());
        $laVue->setUneRepresentation($uneRep);
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - representation");
        $this->vue->afficher();
    }

    /** controleur= representation & action=validerCreer
     * ajouter d'un representation dans la base de données d'après la saisie    */
    public function validerCreer() {
        Bdd::connecter();
        $lieu = null;
        $groupe = null;
        $id = $_REQUEST['id'];
        $id_lieu = $_REQUEST['id_lieu'];
        $id_groupe = $_REQUEST['id_groupe'];
        $daterep = $_REQUEST['daterep'];
        $heuredebut = $_REQUEST['heuredebut'];
        $heurefin = $_REQUEST['heurefin'];


        if ($idlieu == "" || $id_groupe == "") {
            GestionErreurs::ajouter('Chaque champ suivi du caractère * est obligatoire');
            header("Location: index.php?controleur=representations&action=creer");
        } else {
            $lieu = LieuDAO::getOneById($id_lieu);
            $groupe = GroupeDAO::getOneById($id_groupe);

            /* @var representation $unEtab  : récupération du contenu du formulaire et instanciation d'un representation */
            $uneRep = new Representation($id, $lieu, $groupe, $daterep, $heuredebut, $heurefin);


            // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
            // pour un formulaire de création (paramètre n°1 = true)
            $this->verifierDonneesRep($uneRep, true);
            if (GestionErreurs::nbErreurs() == 0) {
                // s'il ny a pas d'erreurs,
                // enregistrer l'representation
                RepresentationDAO::insert($uneRep);
                // revenir à la liste des representation
                header("Location: index.php?controleur=representations&action=liste");
            } else {
                // s'il y a des erreurs, 
                // réafficher le formulaire de création
                $laVue = new VueSaisieRepresentations();
                $this->vue = $laVue;
                $laVue->setActionRecue("creer");
                $laVue->setActionAEnvoyer("validerCreer");
                $laVue->setMessage("Nouvel representation");
                $leGroupe = new Groupe("", "", "", "", "", "", "");
                $leLieu = new Lieu("", "", "", "");
                // En création, on affiche un formulaire vide
                /* @var Representation $uneRep */
                $uneRep = new Representation("", "", "", "", $leLieu, $leGroupe);
                Bdd::connecter();
                $laVue->setLesLieux(LieuDAO::getAll());
                $laVue->setLesGroupes(GroupeDAO::getAll());

                $laVue->setUneRepresentation($uneRep);
                parent::controlerVueAutorisee();
                $laVue->setTitre("Festival - representaion");
                $this->vue->afficher();
            }
        }
    }

    /** controleur= representation & action=modifier $ id=identifiant de l'representation à modifier
     * Afficher le formulaire de modification d'un representation     */
    public function modifier() {
        $idRep = $_GET["id"];
        $laVue = new VueSaisieRepresentations();
        $this->vue = $laVue;
        // Lire dans la BDD les données de l'representation à modifier
        Bdd::connecter();
        $laVue->setLesLieux(LieuDAO::getAll());
        $laVue->setLesGroupes(GroupeDAO::getAll());
        /* @var representation $lerepresentation */
        $laRepresentation = RepresentationDAO::getOneById($idRep);
        $this->vue->setUneRepresentation($laRepresentation);
        $laVue->setActionRecue("modifier");
        $laVue->setActionAEnvoyer("validerModifier");
        $laVue->setMessage("Modifier la representation : " . $laRepresentation->getId() . " (" . $laRepresentation->getId() . ")");
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - representation");;
        $this->vue->afficher();
    }

    /** controleur= representation & action=validerModifier
     * modifier un representation dans la base de données d'après la saisie    */
    public function validerModifier() {
        Bdd::connecter();
        $lesLieux = LieuDAO::getAll();
        $lesGroupes = GroupeDAO::getAll();
        /* @var representation $unEtab  : récupération du contenu du formulaire et instanciation d'un representation */
        $unLieu = LieuDAO::getOneById($_REQUEST['id_lieu']);
        $unGroupe = GroupeDAO::getOneById($_REQUEST['id_groupe']);
        $uneRep = new Representation($_REQUEST['id'], $_REQUEST['daterep'], $_REQUEST['heuredebut'], $_REQUEST['heurefin'], $unLieu, $unGroupe);

        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de modification (paramètre n°1 = false)
        $this->verifierDonneesRep($uneRep, false);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer les modifications pour l'representation
            RepresentationDAO::update($uneRep->getId(), $uneRep);
            // revenir à la liste des representation
            header("Location: index.php?controleur=representations&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de modification
            $laVue = new VueSaisieRepresentations();
            $this->vue = $laVue;
            $laVue->setLesLieux($lesLieux);
            $laVue->setLesGroupes($lesGroupes);
            $laVue->setUneRepresentation($uneRep);
            $laVue->setActionRecue("modifier");
            $laVue->setActionAEnvoyer("validerModifier");
            $laVue->setMessage("Modifier l'représentation : " . $uneRep->getLieu()->getNom() . " (" . $uneRep->getGroupe()->getNom() . ")");
            parent::controlerVueAutorisee();
            $laVue->setTitresetUneRepresentation("Festival - representations");
            $this->vue->afficher();
        }
    }

    /** controleur= representation & action=supprimer & id=identifiant_representation
     * Supprimer un representation d'après son identifiant     */
    public function supprimer() {
        $idRep = $_GET["id"];
        $this->vue = new VueSupprimerRepresentations();
        // Lire dans la BDD les données de l'representation à supprimer
        Bdd::connecter();
        $this->vue->setUneRepresentation(RepresentationDAO::getOneById($idRep));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - representation");
        $this->vue->afficher();
    }

    /** controleur= representation & action= validerSupprimer
     * supprimer un representation dans la base de données après confirmation   */
    public function validerSupprimer() {
        Bdd::connecter();
        if (!isset($_GET["id"])) {
            // pas d'identifiant fourni
            GestionErreurs::ajouter("Il manque l'identifiant de la representation à supprimer");
        } else {
            // suppression de l'representation d'après son identifiant
            RepresentationDAO::delete($_GET["id"]);
        }
        // retour à la liste des representation
        header("Location: index.php?controleur=representations&action=liste");
    }

    
    

}
